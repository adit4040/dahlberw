#Assignment 8
#Part 1
def perimeter_sq(side):
    return (int(side) * 4)
side = input("What is the measurement of one side of the square? ")
print("The perimeter of the square is:", perimeter_sq(side))
print("~"*36)
#Part 2
def area_triangle(base,height):
    return (base * height) / 2
base = int(input("What is the base of the triangle? "))
height = int(input("What is the height of the triangle? "))
print("The area of the triangle is:", area_triangle(base,height))
print("~"*36)
#Part 3
def total_cost(units,price):
    return (units * price)
units = int(input("What is the number of units being sold? "))
price = float(input("What is the price per unit sold? "))
print("The total cost of materials is",total_cost(units,price))
