#Assignment 7
#Part 1
print("~"*36)
print("This is the 9 times table: ")
for num in range(1,13):
    print(num*9)
print("~"*36)

#Part 2
n=int(input('Please enter a number between 1 and 10: '))
if 1 <= n <= 10:
    message = "This is the {} times table: "
    print(message.format(n))
    for num in range(1,13):
        print(num*n)
else:
    print("This is not a valid entry!")
print("~"*36)

#Part 3
n = " "
sum = 0
while n != 0:
    n = int(input("Please enter any number to be added to itself. Enter 0 to stop "))
    sum = sum + n
    print("The total is: " + str(sum))
print("~"*36)

#Part 4
num_laps = 0
total_laps = 24
answer = 'no'
name = input("What is your name? ")
print("Ok {}, you need to run 24 laps on this track in order to run 6 miles. Let's start!".format(name))
while answer == "no":
    rem_laps = total_laps - num_laps
    num_laps = num_laps + 1
    answer = input("Are you done yet? Answer yes or no: ")
if num_laps == total_laps:
	print ("Nice job {}, you fly like an eagle!".format(name))
else:
	print("You are in trouble, go back to work. You have {} more laps left!".format(rem_laps))
