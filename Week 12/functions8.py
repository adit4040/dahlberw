#Bill Dahlberg Assignment 12

from flask import Flask
app=Flask(__name__)

#square route
@app.route('/square/<int:side1>/<int:side2>/<int:side3>/<int:side4>')
def perimeter_square(side1,side2,side3,side4):
	if side1!=side2 or side1!=side3 or side1!=side4 or side2!=side3 or side2!=side4 or side3!=side4:
		return "<h2>These measurements do not represent the shape of a square!</h2>"
	else:
		return "<h2>The perimeter of this square is {} units.</h2>".format(side1+side2+side3+side4)

#triangle route
@app.route('/triangle/<int:base>/<int:height>')
def area_tri(base,height):
	return "<h3> The area of the triangle with a base of {} units, height of {} units, is {} units squared.</h3>".format(base,height,(base*height)/2)

#cost route
@app.route('/totalcost/<int:items>/<int:price>')
def total_cost(items,price):
	return "<h1> The total cost of {} items at a cost of ${} each is ${} </h1>".format(items,price,items*price)

app.run(debug=True, port=8000, host='0.0.0.0')
