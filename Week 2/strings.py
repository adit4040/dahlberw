#BillDahlbergstrings
#1
artist = "Fleetwood Mac"
song = " Dreams"
line1 = " Now here you go again, you say You want your freedom"
line2 = " Well who am I to keep you down It's only right that you should"
line3 = " Play the way you feel it But listen carefully to the sound Of your loneliness"
line4 = " Like a heartbeat drives you mad In the stillness of remembering what you had And what you lost, and what you had, and what you lost"
print(artist)
print(song)
print(line1)
print(line2)
print(line3)
print(line4)

#2
print(" ")

info = artist + song
content = line1 + line2 + line3 + line4
print(info)
print(content)

#3
print(" ")
length = len(content)
message = "One of my favorite songs is {} by {}, and the first part of the song is {} characters long."
print(message.format(song,artist,length))
