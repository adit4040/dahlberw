#BillDahlbergnumbers
#Problem1
print("Problem1")
myage = 26
days_per_year = 365
weeks_per_year = 52
age_weeks = myage * weeks_per_year
age_days = myage * days_per_year
prob1message = "I am {} years old, which is {} weeks old or {} days old."
print(prob1message.format(myage,age_weeks,age_days))

#Problem2
print(" ")
print("Problem2")
distance = 216
avg_speed = 15
time = distance/avg_speed
prob2message = "I am trying to reach NYC by bike, and will be travelling a distance of {} miles. Given my average speed of {} miles per hour, I will most likely reach NYC in {} hours."
print(prob2message.format(distance,avg_speed,time))

#Problem3
print(" ")
print("Problem3")
attendance_lastyear = 12000
attendance_thisyear = 18000
passcost_lastyear = 250
passcost_thisyear = 299
revenue_lastyear = passcost_lastyear*attendance_lastyear
revenue_thisyear = passcost_thisyear*attendance_thisyear
prob3message= "Boston Calling is expecting {} more festival attendees this year. They earned ${} dollars last year, and they expect to earn ${} this year. Compared to last year, this is an increase of ${}."
print(prob3message.format(attendance_thisyear-attendance_lastyear,revenue_lastyear,revenue_thisyear,revenue_thisyear-revenue_lastyear))
