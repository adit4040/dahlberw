#Midterm Project 2 - Guess the Number

print("Ask yourself, 'Do I feel lucky?' Well, do ya, punk? \n -Dirty Harry, 1971 \n")
print("Welcome to the lucky number game! Think you can guess the correct number between 1 and 100? \n")
import random
number=random.randint(1,100)
guessinput = " "

while guessinput!="quit":
    try:
        guessinput = input("Try your best... what do you think the number is? \n")
        if guessinput == "quit":
            print("Only losers quit! Run me again and give it another try!")
            break
        elif int(guessinput)==number:
            print("Congratulations, you've managed to outsmart a basic Python program! As a reward, you never have to play this game again!")
            break
        elif int(guessinput)>100:
            print("Pay attention to the rules! That number is too high.")
        elif int(guessinput)<0:
            print("Pay attention to the rules! Negative numbers aren't included!")
        elif int(guessinput)>number:
            print("You're a bit too high. Guess lower.")
            if abs(number-int(guessinput))>=25:
                print("Also, you're not even close!")
        elif 5<=abs(number-int(guessinput))<=15:
            print("But, you're getting closer...")
        elif abs(number-int(guessinput))<=5:
            print("But, you're so close!")
        elif int(guessinput)<number:
            print("You're a bit too low. Guess higher.")
            if abs(number-int(guessinput))>=25:
                print("Also, you're not even close!")
            elif 5<=abs(number-int(guessinput))<=15:
                print("But, you're getting closer...")
            elif abs(number-int(guessinput))<=5:
                print("But, you're so close!")
    except ValueError:
        print("This is not a valid input. You need to enter a number between 1 and 100, or 'quit' to end the game.")
        continue
