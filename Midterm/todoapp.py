#Midterm Project 1: Todo list app
#create txt file for list items
print("Welcome to the ToDo List application! \n")
try:
    file=open("todo_list.txt","x")
    file.close()
except FileExistsError:
    file=open("todo_list.txt","r")
    lines=file.readlines()
    if len(lines)!=0:
        print("These are the items you have left to do. You can do it... Don't live for tomorrow what you can do today!")
        for line in lines:
            print(line)
    file.close()


#define menu function
def print_menu():
    print(30*"~","Menu",30*"~","\n")
    print("1. Main Menu \n")
    print("2. Show the current items in my ToDo list \n")
    print("3. Delete an item from my ToDo list \n")
    print("4. Quit - save my entries and exit the application \n")
    print(67*"~")

#code for each menu item
entry = " "
while entry !="4":
    print_menu()
    entry = input("Enter text to place an item on your ToDo list. To pick a menu item, enter [1-4]: ")

    if entry=="1":
        print("ToDo List - Main Menu")
        print_menu()
    elif entry=="2": #shows existing list items
        print("Here are my pending ToDo items: \n")
        file=open("todo_list.txt","r")
        lines=file.readlines()
        for line in lines:
            print(line)
        file.close()
    elif entry=="3": #this removes the user inputted entry and rewrites the other entries back to the file
        file=open("todo_list.txt","r+")
        lines=file.readlines()
        file.seek(0)
        delete=input("Enter the item you wish to remove from your list: ")
        for line in lines:
            if line != str(delete) + "\n":
                file.write(line)
        file.truncate()
        file.close()

    elif entry=="4": #Quits the app
        print("Thank you for using the ToDo List app!")

    else: #Adds new entries to ToDo list
        file=open("todo_list.txt","a")
        file.write(entry + "\n")
        file.close()
