#Midterm Project 3
print(15*"~","Best (21+) Vending Machine Ever!",15*"~","\n")
inventory={'vodka':7,'bourbon':7,'beer':5,'chips':2,'pretzels':2,'mystery meat':3}
selections=[]
total=0


while True: #check for user's age - I added this last minute, not sure it's properly coded
    age=''
    age=int(input("What is your age? \n"))
    if age>=21:
        print("Congratulations! You are old enough to use this vending machine! \n")
        break
    else:
        print("You are not old enough to purchase from this vending machine!")
        exit()



def print_menu(): #defines user friendly navigation
    print(30*"~","Menu",30*"~","\n")
    print("Options: \n")
    print("menu - display menu \n")
    print("inventory - view/select available food/beverage items and prices \n")
    print("selections - view selected vending machine items \n")
    print("checkout - view total and purchase items \n")
    print("quit - clear selections and leave the machine \n")
    print(67*"~")
entry=''
print_menu()
while entry !="quit":

    entry=input("Please make a selection. You can enter one of the menu options, or a vending selection.\nEnter 'help' at any time to see the main menu. \n")
    if not inventory:
        print(10*"~")
        print("Sold out. Please come again!")
        print("You have selected {} items, totalling ${}".format(len(selections),total))
        print(10*"~")
        break
    if entry == "help":
        print_menu()

    elif entry == "inventory":
        if not inventory:
            print(10*"~")
            print("Sold out. Please come again!")
            print(10*"~")
        else:
            print("Available items and prices:")
            for item in inventory:
                print(10*"~")
                print(item + " $"+str(inventory[item]))
                print(10*"~")
    elif entry== "quit":
        break
    elif entry=="selections":
        if not selections:
            print(10*"~")
            print("You haven't selected any items.")
            print(10*"~")
        else:
            print(10*"~")
            print("You have selected the following item(s):" + str(selections))
            print("Your current total is ${}".format(total))
            print(10*"~")
    elif entry =="checkout":
        if not selections:
            print(10*"~")
            print("You haven't selected any items. Please enter an item to purchase, or enter 'quit' to leave the vending machine.")
            print(10*"~")
        else:
            print(10*"~")
            print("You are purchasing {} item(s) for a total of ${}".format(len(selections),total))
            selections=[]
            total=0
            print("Select additional items or enter 'quit' to leave the vending machine.")
            print(10*"~")
    elif entry in inventory :
        selections.append(entry)
        print(10*"~")
        print("You have selected the following item(s):" + str(selections))
        print(5*"~")
        print("Item cost is ${}".format(inventory[entry]))
        total+=inventory[entry]
        print(5*"~")
        print("Your current total is ${}".format(total))
        del inventory[entry]
        print(10*"~")
    else:
        print(10*"~")
        print("This is not a valid input.")
        print(10*"~")

print("Your vending machine experience has come to an end. Have a great day!")
