#Bill Dahlberg - Final Project

import logging
from random import randint
from flask import Flask, render_template
from flask_ask import Ask, statement, question, session

app = Flask(__name__)
ask = Ask(app, "/")
logging.getLogger("flask_ask").setLevel(logging.DEBUG)

@ask.launch
def welcomeMessage():
    return question("Welcome to the lucky number game! Think you can guess the correct number between 1 and 100?")

@ask.intent("NoIntent")
def goodbyeMessage():
    return statement("Thanks for trying. Goodbye.")

@ask.intent("YesIntent")
def next_round():
    number =  randint(1, 100)
    session.attributes['winning_number'] = number
    return question("OK! Try your best, what do you think the lucky number is?")

@ask.intent("AnswerIntent", convert={'first': int})
def answer(first):
    try:
        winning_number = session.attributes['winning_number']
        if first == winning_number:
            return question("You picked the right number, this time. Good job! Do you want to play again?")
        elif first != winning_number:
            if int(first)>100:
                return question("Pay attention to the rules! That number is too high. Guess again.")
            elif int(first)<0:
                return question("Pay attention to the rules! Negative numbers aren't included!")
            elif int(first)>winning_number and abs(winning_number-int(first))>=25:
                        return question("You're a bit too high, not even close. Guess lower. ")
            elif int(first)>winning_number and abs(winning_number-int(first))>=15:
                        return question("You're a bit too high, but you're getting closer. Guess lower. ")
            elif int(first)>winning_number and 5<=abs(winning_number-int(first))<=15:
                return question("You're a bit too high, but you're warm. Guess lower.")
            elif int(first)>winning_number and abs(winning_number-int(first))<=5:
                return question("You're a bit too high, but you're very close! Guess lower.")
            elif int(first)<winning_number and abs(winning_number-int(first))>=25:
                        return question("You're a bit too low, not even close. Guess higher. ")
            elif int(first)<winning_number and abs(winning_number-int(first))>=15:
                        return question("You're a bit too low, but you're getting closer. Guess higher. ")
            elif int(first)<winning_number and 5<=abs(winning_number-int(first))<=15:
                return question("You're a bit too low, but you're warm. Guess higher.")
            elif int(first)<winning_number and abs(winning_number-int(first))<=5:
                return question("You're a bit too low, but you're very close! Guess higher.")
            else:
                return question("That is not a valid response. Try again.")
    except ValueError:
        return question("That is not a valid response. Try again.")

if __name__=='__main__':
    app.run(debug=True)
