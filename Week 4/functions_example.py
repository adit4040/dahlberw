#Problem 1
def age_indays(age):
    return (int(age) * 365)
age = input("What is your age?" )
print("Your age in days is:", age_indays(age))

#Problem 2
def monthly_bill(gas, elec, cable, internet, phone):
    total = gas + elec + cable + internet + phone
    return total

gas = int(input("What's the gas bill?" ))
elec = int(input("What's the electric bill?" ))
cable = int(input("What's the cable bill?" ))
internet = int(input("What's the internet bill?" ))
phone = int(input("What's the phone bill?" ))
print("Your monthly bill is:", monthly_bill(gas, elec, cable, internet, phone))
