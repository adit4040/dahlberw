#Problem 1
list(range(1,30))
for num in list(range(1,31)):
    print(num*num*num)
print(" ")
print("~"*30)
#Problem 2
list(range(1,20))
for num in list(range(1,21)):
    if num % 2 == 0:
        print(num)
print(" ")
print("~"*30)
#Problem 3
list(range(1,101))
for num in list(range(1,101)):
    if num == 24:
        continue
    elif num  == 81:
        break
    print(num)
