#Assignment 5 - Lists

#Questions and Answers lists
questions_list = ["Are you chewing on life's gristle?", "Does your life seem jolly rotten?", "Are you feeling in the dumps?", "Does your life seem quite absurd?"]
answers_list = ["Don't grumble, give a whistle!","There's something you've forgotten, And that's to laugh and smile and dance and sing!", "Don't be silly chumps, Just purse your lips and whistle -- that's the thing!", "Death's the final word, You must always face the curtain with a bow!"]

#Part 1 - User input questions and answers

input(questions_list[0]+ " ")
print(answers_list[0]+ " ")
print("\n")
input(questions_list[1]+ " ")
print(answers_list[1]+ " ")
print("\n")
input(questions_list[2]+ " ")
print(answers_list[2]+ " ")
print("\n")
input(questions_list[3]+ " ")
print(answers_list[3]+ " ")
print("\n")
print("~"*30)

#Part 2 - Boolean Values
responses_list = ["True", "False"]
print("If your answer to these questions is yes, respond with True !")
print("\n")
question1 = input(questions_list[0] + " ")
if question1 == responses_list[0]:
    print(answers_list[0] + " ")
else:
    print("Then good for you!")
print("\n")

question2 = input(questions_list[1] + " ")
if question2 == responses_list[0]:
    print(answers_list[1] + " ")
else:
    print("Then good for you!")
print("\n")

question3 = input(questions_list[2] + " ")
if question2 == responses_list[0]:
    print(answers_list[2] + " ")
else:
    print("Then good for you!")
print("\n")

question4 = input(questions_list[3] + " ")
if question2 == responses_list[0]:
    print(answers_list[3] + " ")
else:
    print("Then good for you!")
print("~"*30)
