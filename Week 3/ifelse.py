#Assignment 6 - If/elif/else
#Superbowl 2017
falcons_score = 28
print("It's Superbowl Sunday! The Falcons score is 28 points on the fourth quarter of the game with a minute to play.")
patriots_score = input("What's the current Patriots score? \n")
if int(falcons_score)-int(patriots_score) == 0:
    print("The Patriots need to score a field goal!")
elif int(falcons_score)-int(patriots_score) == 6:
    print("The Patriots need to score a touch down and kick through the uprights!")
elif int(falcons_score)-int(patriots_score) == 7:
    print("The Patriots need to score a touch down and a field goal!")
elif int(falcons_score)-int(patriots_score) >= 8:
    print("We are doomed. Goodbye Tom Brady")
elif int(falcons_score)-int(patriots_score) <= -1:
    print("Let's go Patriots!")
