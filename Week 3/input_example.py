#Bill Dahlberg Week 3 Problem 1
#Interview Questions
question1 = input("Describe the project you've worked on that you're most proud of. What did you do that worked out particularly well? \n")
print(" ")
question2 = input("Describe the project you've worked on that you're least proud of. What would you do differently? \n")
print(" ")
question3 = input("Give me an example of a time you had to take a creative and unusual approach to solve coding problem. \n")
print(" ")
#Interview Answers
answer1 = "The answer to question 1 is {}"
print(answer1.format(question1))
answer2 = "The answer to question 2 is {}"
print(answer2.format(question2))
answer3 = "The answer to question 3 is {}"
print(answer3.format(question3))
print(" ")
#Age question
agequestion = input("How many years have you been on the Earth? \n")
ageanswer = int(agequestion) * 52
agemessage = "This translates to {} weeks on Earth"
print(agemessage.format(ageanswer))
