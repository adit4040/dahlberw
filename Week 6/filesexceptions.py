#Assignment 9

file=open("seasons.txt","w+")
file.close()
file=open("seasons.txt","a")


#Clears old file inputs, opens file in append mode
answers=["winter","spring","summer","fall","quit"]
answer=''
print("When prompted below, please enter your favorite season in lowercase form. Enter 'quit' when finished to see the tally!\n")
while answer !="quit":
	answer=input("What's your favorite season? ")
	if answer not in answers:
		print("That's not a valid response! See intructions above.")
	elif answer=="quit":
		break
	else:
		file.write(answer+"\n")

#Closes file, reopens in read mode
file.close()
file=open("seasons.txt","r")
fileinput=file.readlines()

winter=0
spring=0
summer=0
fall=0

for line in fileinput:
	if line=="winter\n":
		winter+=1
	elif line=="spring\n":
		spring+=1
	elif line=="summer\n":
		summer+=1
	elif line=="fall\n":
		fall+=1

seasonsinput=[]
for line in fileinput:
	seasonsinput.append(line)

#Display results of user entries
print("\n")
print("Here is the current tally for favorite seasons: ")
print("Winter: "+ str(winter))
print("Spring: " + str(spring))
print("Summer: "+ str(summer))
print("Fall: "+ str(fall))


file.close()
